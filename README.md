### 个人信息     
- 姓名：武鹏飞             
- 生日：1992.07.04 
- 现居：北京 · 昌平  
- 电话：155 1011 9444    
- 邮箱：wu.pengfei55555@163.com
### 教育背景
- 2012.09-2016.07       吉林农业科技学院       本科学士
### 求职意向
- 工作性质：全职
- 期望职业：python爬虫工程师
- 期望行业：互联网/数据服务
- 工作地区：北京
- 期望薪资：面议
- 目前状况：已离职，到岗时间面议
### 工作经历
- 2016.04 -- 2018.01     中软国际有限公司    python工程师
### 专业技能


1. 熟悉 Linux操作系统；
1. 熟悉python语言；
1. 熟悉爬虫过程，掌握使用Requests包爬虫技术以及scrapy框架；
1. 了解熟悉Python 多线程爬虫及其机制；
1. 了解re、xpath、BeautifulSoup4数据提取技术；
1. 了解Selenium+PhantomJS动态HTML抓取；
1. 了解mysql、mongdb、redis操作；
1. 了解HTTP/HTTPS协议，TCP/IP网络协议；
1. 遵循PEP8规范。
### 个人项目
1. SuningSpider - 苏宁图书爬虫。爬取苏宁图书全站图书信息，通过构建url，实现对全站图书列表的抓取，并通过DuplicatesPipeline管道对item去重保存至本地json文件。
1. SinaSpider - 新浪微博爬虫。主要爬取新浪微博用户的个人信息、微博信息、粉丝和关注。代码获取新浪微博 Cookie 进行登录，可通过多账号登录来防止新浪的反扒。主要使用 scrapy 爬虫框架。 
1. distribute_crawler - 小说下载分布式爬虫。使用 scrapy,Redis, MongoDB,graphite 实现的一个分布式网络爬虫,底层存储 mongodb 集群,分布式使用 redis 实现,爬虫状态显示使用 graphite 实现，主要针对一个小说站点。
1. LianJiaSpider - 链家网爬虫。爬取北京地区链家历年二手房成交记录。涵盖链家爬虫一文的全部代码，包括链家模拟登录代码。
1. scrapy_jingdong - 京东爬虫。基于 scrapy 的京东网站爬虫，保存格式为 csv。
1. QunarSpider - 去哪儿网爬虫。 网络爬虫之 Selenium 使用代理登陆：爬取去哪儿网站，使用 selenium 模拟浏览器登陆，获取翻页操作。代理可以存入一个文件，程序读取并使用。支持多进程抓取。
1. findtrip - 机票爬虫（去哪儿和携程网）。Findtrip 是一个基于 Scrapy 的机票爬虫，目前整合了国内两大机票网站（去哪儿 + 携程）。
1. 163spider  - 基于 requests、MySQLdb、torndb 的网易客户端内容爬虫
1. doubanspiders- 豆瓣电影、书籍、小组、相册、东西等爬虫集
1. tbcrawler- 淘宝和天猫的爬虫,可以根据搜索关键词,物品 id 来抓去页面的信息，数据存储在mongodb。
### 自我评价
1. 具备优秀的文档阅读能力，能流畅阅读各种API技术文档等；
1. 学习能力强，思路清晰，对新技术有一定的渴望；
1. 为人踏实，能快速地融入团队，服从领导安排的各项任务；
1. 良好的沟通能力，积极主动，对工作尽心尽责，抗压能力强