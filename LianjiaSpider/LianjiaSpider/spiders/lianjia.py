# -*- coding: utf-8 -*-
import scrapy
from LianjiaSpider.items import LianjiaspiderItem
import re


class LianjiaSpider(scrapy.Spider):
    name = 'lianjia'
    allowed_domains = ['lianjia.com']
    start_urls = ['https://bj.lianjia.com/ershoufang/']

    #获取大分类
    def parse(self, response):
        area_list = response.xpath("//div[@data-role='ershoufang']/div[1]/a")
        for area in area_list:
            item = LianjiaspiderItem()
            area_url = "https://bj.lianjia.com" + area.xpath("./@href").extract_first()
            area_l = area.xpath("./text()").extract_first()
            item["area_l"] = area_l
            yield scrapy.Request(
                area_url,
                callback=self.parse_area,
                meta={"item":item}
            )

    #获取小分类
    def parse_area(self,response):
        area_list = response.xpath("//div[@data-role=\"ershoufang\"]/div[2]/a")

        for area in area_list:
            item = response.meta["item"]
            area_url = "https://bj.lianjia.com" + area.xpath("./@href").extract_first()
            area_s = area.xpath("./text()").extract_first()
            item["area_s"] = area_s
            yield scrapy.Request(
                area_url,
                callback=self.parse_info,
                meta={"item":item,
                      "url_temp":area_url}
            )

    #获取房屋信息
    def parse_info(self,response):
        house_list = response.xpath("//li[@class='clear']")
        for house in house_list:
            item = response.meta["item"]
            region = house.xpath("./div/div[2]/div/a/text()").extract_first()
            type = house.xpath("./div/div[2]/div/text()[1]").extract_first()
            acreage = house.xpath("./div/div[2]/div/text()[2]").extract_first()
            orientation = house.xpath("./div/div[2]/div/text()[3]").extract_first()
            decoration = house.xpath("./div/div[2]/div/text()[4]").extract_first()
            estate = house.xpath("./div/div[3]/div/text()[1]").extract_first()
            floor_type = house.xpath("./div/div[3]/div/text()[2]").extract_first()
            care = house.xpath("./div/div[4]/text()[1]").extract_first()
            see = house.xpath("./div/div[4]/text()[2]").extract_first()
            tag = house.xpath("./div/div[4]/div[2]/span/text()").extract()
            total = house.xpath("./div/div[4]/div[3]/div[1]/span/text()").extract_first()
            total += "万"
            price = house.xpath("./div/div[4]/div[3]/div[2]/span/text()").extract_first()
            item["region"] = region
            item["type"] = type
            item["acreage"] = acreage
            item["orientation"] = orientation
            item["decoration"] = decoration
            item["estate"] = estate
            item["floor_type"] = floor_type
            item["care"] = care
            item["see"] = see
            item["tag"] = tag
            item["total"] = total
            item["price"] = price
            yield item

        # 获取其他页
        total_page = re.findall(r'\"totalPage\"\:(.*?),',response.body.decode(),re.S)[0]
        if int(total_page)>1:
            for i in range(1,int(total_page)):
                page_url = response.meta["url_temp"] + "pg{}/".format(i+1)
                yield scrapy.Request(
                    page_url,
                    callback=self.parse_info,
                    meta=response.meta
                )




