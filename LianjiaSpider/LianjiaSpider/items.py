# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class LianjiaspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    area_l = scrapy.Field() #大区域
    area_s = scrapy.Field() #小区域
    region = scrapy.Field() #小区名称
    type = scrapy.Field() #户型
    acreage = scrapy.Field() #面积
    orientation = scrapy.Field() #朝向
    decoration = scrapy.Field() #装修
    estate = scrapy.Field() #楼层
    floor_type = scrapy.Field() #建造时间&楼型
    see = scrapy.Field() #带看
    care = scrapy.Field() #关注
    tag = scrapy.Field() #标签
    total = scrapy.Field() #总价
    price = scrapy.Field() #单价


