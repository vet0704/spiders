# -*- coding: utf-8 -*-
import re

import scrapy
from SuningSpider.items import SuningItem


class SuningSpider(scrapy.Spider):
    name = "suning"
    allowed_domains = ["suning.com"]
    start_urls = (
        'http://snbook.suning.com/web/trd-fl/999999/0.htm',
    )

    def parse(self, response):
        book_list = response.xpath("//li[@class='clearfix']")
        for book in book_list:
            item = SuningItem()
            url = book.xpath("./div[1]/a/@href").extract_first()
            name = book.xpath("./div[2]/div[@class='book-title']/a/text()").extract_first()
            author = book.xpath("./div[2]/div[2]/div[1]/a/text()").extract_first()
            press = book.xpath("./div[2]/div[2]/div[2]/a/text()").extract_first()
            info = book.xpath("./div[2]/div[3]/text()").extract_first()
            item['url'] = url
            item['name'] = name
            item['author'] = author
            item['press'] = press
            item['info'] = info
            yield scrapy.Request(
                url,
                callback=self.get_content,
                meta={"item": item}
            )

        
        page_number = re.findall("pageNumber=(.*?)&sort=0",response.request.url)
        if len(page_number)==0:
            next_url = "http://snbook.suning.com/web/trd-fl/999999/0.htm" + "?pageNumber=2&sort=0"
            yield scrapy.Request(
                next_url,
                callback=self.parse,
            )
        elif len(page_number)!=0 and int(page_number[0])<459:
            next_url = "http://snbook.suning.com/web/trd-fl/999999/0.htm" + "?pageNumber={}&sort=0".format(int(page_number[0])+1)
            yield scrapy.Request(
                next_url,
                callback=self.parse,
            )

    def get_content(self,response):
        item = response.meta['item']
        tag = response.xpath("//div[@class='parm fl']/a/text()").extract_first()
        price = re.findall(r"\"bp\":\'(.*?)\'",response.body.decode())[0]
        date = response.xpath("//li[@class='clearfix wauto']/div/span[2]/text()").extract_first()
        item["tag"] = tag
        item["price"] = price
        item["date"] = date
        yield item





